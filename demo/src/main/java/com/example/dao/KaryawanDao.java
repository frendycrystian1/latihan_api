package com.example.dao;

import com.example.model.*;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.*;

@Repository
public interface KaryawanDao extends JpaRepository<Karyawan, Long>{

}
