package com.example.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.example.dao.*;
import com.example.model.Karyawan;
import com.example.exception.ResourceNotFoundException;

@RestController
public class KaryawanController {
	@Autowired
	KaryawanDao karyawanDao;

	@GetMapping("/karyawan")
	public Page<Karyawan> getKaryawan(Pageable pageable) {
		return karyawanDao.findAll(pageable);
	}

	// buat tambah
	@PostMapping("/Karyawan/add")
	public String addKaryawan(@Valid @RequestBody Karyawan karyawan) {
		karyawanDao.save(karyawan);
		return "succress";
	}

	// buat update
	@PutMapping("/Karyawan/update/{karyawanId}")
	public Karyawan updateKaryawan(@PathVariable Long karyawanId, @Valid @RequestBody Karyawan karyawanRequest) {
		return karyawanDao.findById(karyawanId).map(karyawann -> {
			karyawann.setName(karyawanRequest.getName());
			karyawann.setAddress(karyawanRequest.getAddress());
			karyawann.setPhone(karyawanRequest.getPhone()); // phone
			karyawann.setEmergency(karyawanRequest.getEmergency());
			karyawann.setSalary(karyawanRequest.getSalary());
			karyawann.setResignStatus(karyawanRequest.getResignStatus());
			karyawann.setResignAt(karyawanRequest.getResignAt());
			return karyawanDao.save(karyawann);
		}).orElseThrow(() -> new ResourceNotFoundException("karyawan not found, with id " + karyawanId));

	}
	//buat delete
	@DeleteMapping("/karyawan/delete/{karyawanId}")
	public ResponseEntity<?> deleteKaryawan(@PathVariable Long karyawanId) {
		return karyawanDao.findById(karyawanId).map(karyawann -> {
			karyawanDao.delete(karyawann);
			return ResponseEntity.ok().build();
		}).orElseThrow(() -> new ResourceNotFoundException("karyawan not found, with id " + karyawanId));
	}
}
