package com.example.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.DynamicInsert;
@Entity
@Table(name="karyawan")
@DynamicInsert
public class Karyawan implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="karyawan", columnDefinition = "VARCHAR(255)")
	@NotNull
	private String name;
	
	@Column(name="address", columnDefinition = "VARCHAR(255)")
	private String address;
	
	@Column(name="phone", columnDefinition = "VARCHAR(14)")
	private String phone;
	
	@Column(name="emergency", columnDefinition = "VARCHAR(155)", nullable = true)
	private String emergency; 
	
	@Column(name="salary") 
	private int salary;
	
	@Column(name="startat")
	private Date startAt;
	
	@Column(name="resignstatus", nullable = true)
	private String resignStatus;
	
	@Column(name="resignat", nullable = true)
	private Date resignAt;
	
	@Column(name="createat", columnDefinition = "TIMESTAMP default NOW()")
	private Date createAt;
	
	@Column(name="createby", columnDefinition = "VARCHAR(255) default 'SYSTEM' ")
	private String createBy;
	
	@Column(name="updateat", columnDefinition = "TIMESTAMP default NOW()")
	private Date updateAt;
	
	@Column(name="updateby", columnDefinition = "VARCHAR(255) default 'SYSTEM' ")
	private String updateBy;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmergency() {
		return emergency;
	}

	public void setEmergency(String emergency) {
		this.emergency = emergency;
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}

	public Date getStartAt() {
		return startAt;
	}

	public void setStartAt(Date startAt) {
		this.startAt = startAt;
	}

	public String getResignStatus() {
		return resignStatus;
	}

	public void setResignStatus(String resignStatus) {
		this.resignStatus = resignStatus;
	}

	public Date getResignAt() {
		return resignAt;
	}

	public void setResignAt(Date resignAt) {
		this.resignAt = resignAt;
	}
	
}
